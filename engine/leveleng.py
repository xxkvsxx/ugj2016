#!/usr/bin/python3

import pickle
from hashlib import md5
from engine import achievements
import copy

class LevelEng():
    def __init__(self):
        self.__all = [
            {
                "name": "I. Обучение",
                "levels": 5
            },
            {
                "name": "II. Знакомство",
                "levels": 4
            },
            {
                "name": "III. Общение",
                "levels": 4
            },
            {
                "name": "IV. Вопросы??",
                "levels": 2
            },
            {
                "name": "V. Хардкор",
                "levels": 3
            }
        ]
        self.__ach = achievements.Achiv(self)

    def load(self):
        try:
            with open("save", "rb") as fin:
                self.__cur = pickle.load(fin)
        except FileNotFoundError:
            return False
        if (type(self.__cur) is not list) or (len(self.__cur) == 0):
            return False
        if not self.checkmd5():
            return False
        if not self.__ach.load():
            self.__ach.setNewStat()
        self.__load()
        return True

    def checkmd5(self):
        sum1 = self.getmd5()
        sum2 = self.__cur[-1]
        if sum1 != sum2:
            return False
        return True

    def getmd5(self):
        s = self.__cur[:-1]
        return md5(str(s).encode("utf-8")).hexdigest()

    def newCur(self):
        self.__cur = [1, [], 0]
        self.__cur = [1, [], self.getmd5()]
        self.__load()
        if not self.__ach.load():
            self.__ach.setNewStat()
        self.save()

    def getLast(self):
        return len(self.__all)

    def getAll(self):
        return self.__all

    def getCur(self):
        return self.__cur[0]

    def getPassed(self):
        return self.__cur[1]

    def getCurNum(self):
        return self.__curnum

    def __load(self):
        self.__curnum = (self.__cur[0] - 1) // 3 * 3 + 1
        if self.__curnum >= len(self.__all):
            self.__curnum = (len(self.__all) - 1) // 3 * 3 + 1

    def save(self):
        self.__ach.setmd5()
        with open("save", "wb") as fout:
            pickle.dump(self.__cur, fout)

    def openLevel(self, b, n):
        if self.__cur[0] == b:
            if n not in self.__cur[1]:
                self.__cur[1].append(n)
        if len(self.__cur[1]) == self.__all[b-1]["levels"]:
            self.__cur[0] += 1
            self.__cur[1] = []
            self.__load()
        self.__cur[2] = self.getmd5()
        self.save()

    def nextBlockExists(self):
        s = (len(self.__all) - 1) // 3 * 3 + 1
        return (self.__curnum != s)

    def lastBlockExists(self):
        return (self.__curnum != 1)


    def lastBlock(self):
        self.__curnum -= 3

    def nextBlock(self):
        self.__curnum += 3

    def returnBlock(self):
        start = self.__curnum - 1
        end = min(self.__curnum + 2, len(self.__all))
        l = copy.deepcopy(self.__all[start: end])
        r = ([], [])
        start += 1
        for i in range(len(l)):
            if (start+i) < self.__cur[0]:
                l[i]["passed"] = list(range(1, l[i]["levels"]+1))
                r[0].append(l[i])
            elif (start+i) == self.__cur[0]:
                l[i]["passed"] = self.__cur[1]
                r[0].append(l[i])
            else:
                r[1].append(l[i])
        return r

    def getAchiv(self):
        return self.__ach


