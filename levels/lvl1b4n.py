poslst = [i for i in range(15)]
fdescr = [
    'f(0) = f(1) = 1',
    'И почему я вам помогаю?',
    'Решайте, тут всё просто.',
    'Что ты тут хочешь увидеть?',
    'Закрыто на обед!'
]
fpres = 'f;(;x;-;2;);*;x'.split(';')
posfixed = [2,4,5,7]

def f(x, elem):
    if x == 0: return 1
    if x == 1: return 1
    return f(x-2, elem)*x