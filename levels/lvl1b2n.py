poslst = [i for i in range(15)]
fdescr = [
    'Скобка ставится однозначно',
    'X тоже ставится однозначно',
    'Остаются 2 оператора',
    'Обратите внимание на их приоритеты'
]
fpres = 'mxcnt;*;(;x;+;2;)'.split(';')
posfixed = [0,2,5]

def f(x, elem):
    return elem.mxcnt*(x+2)