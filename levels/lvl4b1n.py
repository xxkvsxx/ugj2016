poslst = [i for i in range(15)]
fdescr = [
    'Ни одного коэффициента?',
    'Повезло.'
    '',
    'Кто-нибудь объяснит мне, что такое жизнь?',
    'Что значит быть живым?',
    '',
    'Думаете, я тут в качестве помощника Милтона?',
    'Возможно...',
    ''
]
fpres = '-;x;+;mxcnt;-;maxm;+;minm;-;unic'.split(';')
posfixed = [0,1,2,4,5,6,7]

def f(x, elem):
    return elem.mxcnt-elem.maxm+elem.minm-elem.unic-x