#!/usr/bin/python3

from PyQt4 import QtGui, QtCore

class Button(QtGui.QPushButton):

    def __init__(self, obj, name, width, height, func):
        super(Button, self).__init__()
        self.__back = name
        self.__stl = "background-color: transparent; background-image: url(resource/buttons/{}-{}.png);"
        self.setFixedWidth(width)
        self.setFixedHeight(height)
        obj.connect(self, QtCore.SIGNAL("clicked()"), func)
        self.connect(self, QtCore.SIGNAL('pressed()'), self.backactive)
        self.connect(self, QtCore.SIGNAL('released()'), self.backpassive)
        self.backpassive()

    def backactive(self):
        self.setStyleSheet(self.__stl.format(self.__back, "up"))

    def backpassive(self):
        self.setStyleSheet(self.__stl.format(self.__back, "down"))