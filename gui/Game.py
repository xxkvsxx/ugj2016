#!/usr/bin/python3

from PyQt4 import QtGui
from . import GameScene

class Game(QtGui.QGraphicsView):

    def __init__(self, parent, n1, n2, lvc):
        super(Game, self).__init__()
        self.__parent = parent
        self.__sc = GameScene.GameScene(self, n1, n2, lvc)
        self.__name = "game"
        self.setScene(self.__sc)

    def getName(self):
        return self.__name

    def getGameParent(self):
        return self.__parent

    def getScene(self):
        return self.__sc
