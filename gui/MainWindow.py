#!/usr/bin/python3

from PyQt4 import QtGui
from . import Menu
from . import Game
from . import Level
from . import About
from . import Extra
import sys
import sip

class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setFixedWidth(1120)
        self.setFixedHeight(630)
        self.setWindowTitle("Function Reverse")
        self.setStyleSheet("background-image: url(resource/back.jpg)")
        # Инициализация Стека
        self.__stackw = {}
        self.__stack = QtGui.QStackedWidget()
        self.__stackw["menu"] = Menu.Menu(self)
        self.__stack.addWidget(self.__stackw["menu"])
        self.setCentralWidget(self.__stack)

    def getStack(self):
        return self.__stack

    def switchToLevels(self):
        try:
            sip.delete(self.__stackw["level"].getScene())
            sip.delete(self.__stackw["level"])
        except RuntimeError:
            pass
        except KeyError:
            pass
        self.__stackw["level"] = Level.Level(self)
        self.__stack.addWidget(self.__stackw["level"])
        self.__stack.setCurrentWidget(self.__stackw["level"])

    def switchToGame(self, n1, n2, lvc):
        try:
            sip.delete(self.__stackw["game"].getScene())
            sip.delete(self.__stackw["game"])
        except RuntimeError:
            pass
        except KeyError:
            pass
        self.__stackw["game"] = Game.Game(self, n1, n2, lvc)
        self.__stack.addWidget(self.__stackw["game"])
        self.__stack.setCurrentWidget(self.__stackw["game"])

    def switchToAbout(self):
        try:
            sip.delete(self.__stackw["about"].getScene())
            sip.delete(self.__stackw["about"])
        except RuntimeError:
            pass
        except KeyError:
            pass
        self.__stackw["about"] = About.About(self)
        self.__stack.addWidget(self.__stackw["about"])
        self.__stack.setCurrentWidget(self.__stackw["about"])

    def switchToExtra(self):
        try:
            sip.delete(self.__stackw["extra"].getScene())
            sip.delete(self.__stackw["extra"])
        except RuntimeError:
            pass
        except KeyError:
            pass
        self.__stackw["extra"] = Extra.Extra(self)
        self.__stack.addWidget(self.__stackw["extra"])
        self.__stack.setCurrentWidget(self.__stackw["extra"])

    def returnToMenu(self):
        a = self.__stack.currentWidget()
        self.__stack.setCurrentWidget(self.__stackw["menu"])
        self.__stack.removeWidget(a)

    def some(self):
        pass


    def closeEvent(self, *args, **kwargs):
        self.__stack.currentWidget().getScene().clear()
        sys.exit(0)
