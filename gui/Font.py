#!/usr/bin/python3

from PyQt4 import QtGui, QtCore

class Font(QtGui.QFont):
    def __init__(self, family):
        super(Font, self).__init__()
        self.setFamily(family)
        self.__metr = QtGui.QFontMetrics(self)
        self.__line = QtCore.Qt.TextSingleLine

    def find(self, width, height, text):
        self.__height = height
        self.__width = width
        self.__text = text
        size = self.__metr.size(self.__line, self.__text)
        p = max(self.pixelSize(), 1)
        while (size.height() > self.__height) or (size.width() > self.__width):
            p -= 2
            if p <= 0:
                p = 2
                break
            self.setPixelSize(p)
            self.__metr = QtGui.QFontMetrics(self)
            size = self.__metr.size(self.__line, self.__text)
        while (size.height() <= self.__height) and (size.width() <= self.__width):
            p += 1
            self.setPixelSize(p)
            self.__metr = QtGui.QFontMetrics(self)
            size = self.__metr.size(self.__line, self.__text)
        p -= 1
        self.setPixelSize(p)

    def setLine(self, t):
        self.__line = t

