#!/usr/bin/python3

from PyQt4 import QtGui, QtCore
from . import Button
from engine import leveleng
from . import Font
from functools import partial

class LevelScene(QtGui.QGraphicsScene):

    def __init__(self, parent):
        super(LevelScene, self).__init__(5, 5, 1110, 620)
        self.__parent = parent
        self.__chooser = leveleng.LevelEng()
        if not self.__chooser.load():
            self.__chooser.newCur()
        a = Button.Button(
            self.getLevelSceneParent().getLevelParent(),
            "exitm",
            274,
            74,
            self.getLevelSceneParent().getLevelParent().returnToMenu
        )
        a = self.addWidget(a)
        a.setPos(800, 525)

        a = Button.Button(
            self.getLevelSceneParent().getLevelParent(),
            "r",
            83,
            83,
            self.switchRight
        )
        a = self.addWidget(a)
        a.setPos(1020, 210)
        self.__right = a

        a = Button.Button(
            self,
            "l",
            83,
            83,
            self.switchLeft
        )
        a = self.addWidget(a)
        a.setPos(15, 210)
        self.__left = a

        self.init()

    def init(self):
        self.__left.hide()
        self.__right.hide()
        self.__items = []
        x, y = 0, 0
        r = self.__chooser.returnBlock()
        self.__chooser.getCur()
        for i in range(len(r[0])):
            x, y = self.addGroup(x, y, r[0][i], self.__chooser.getCurNum()+i)
        for i in r[1]:
            x, y = self.addCloseGroup(x, y, i)
        a = QtGui.QProgressBar()
        a.setFixedHeight(50)
        a.setFixedWidth(300)
        a.setMaximum(self.__chooser.getLast())
        a.setMinimum(0)
        a.setValue(self.__chooser.getCur()-1)
        a.setFormat("Пройдено: %v/%m")
        a.setStyleSheet(
            """
            QProgressBar {
                background: transparent;
                text-align: center;
                color: white;
                font-size: 30px;
            }
            QProgressBar:horizontal{
                border: 5px solid black;
                border-radius: 10px;
                background-color: #55238f;
                color: yellow;
            }
            QProgressBar:chunk:horizontal{
            background-color: qlineargradient(x1:0, y1:1, x2: 1, y2:1, stop: 0 #01439a, stop: 0.95 #15378e stop: 1 black);
            }
            """
        )
        a = self.addWidget(a)
        a.setPos(100, 530)
        self.__items.append(a)
        if self.__chooser.nextBlockExists():
            self.__right.show()

        if self.__chooser.lastBlockExists():
            self.__left.show()

    def switchLeft(self):
        self.__chooser.lastBlock()
        for i in self.__items:
            self.removeItem(i)
        self.init()

    def switchRight(self):
        self.__chooser.nextBlock()
        for i in self.__items:
            self.removeItem(i)
        self.init()

    def addGroup(self, x, y, d, b):
        a = self.addPixmap(QtGui.QPixmap("resource/levelblock.png"))
        a.setPos(x+100, y+10)
        self.__items.append(a)
        f = Font.Font("HACKED")
        f.find(720, 50, d["name"])
        a = QtGui.QGraphicsTextItem(d["name"])
        a.setDefaultTextColor(QtGui.QColor("black"))
        a.setFont(f)
        a.setPos(x + 202, y + 17)
        self.addItem(a)
        self.__items.append(a)
        a = QtGui.QGraphicsTextItem(d["name"])
        a.setDefaultTextColor(QtGui.QColor("white"))
        a.setFont(f)
        a.setPos(x+200, y+15)
        self.addItem(a)
        self.__items.append(a)
        for i in range(1, d["levels"]+1):
            a = Button.Button(
                self.getLevelSceneParent().getLevelParent(),
                "l{}".format(i),
                74,
                74,
                partial(self.getLevelSceneParent().getLevelParent().switchToGame, b, i, self.__chooser)
            )
            a = self.addWidget(a)
            a.setPos(x+180+85*(i-1), y+80)
            self.__items.append(a)
            if i in d["passed"]:
                a = self.addPixmap(QtGui.QPixmap("resource/pass.png"))
                a.setPos(x+230+85*(i-1), y+130)
                self.__items.append(a)
        return (x, y+160)

    def addCloseGroup(self, x, y, d):
        f = Font.Font("HACKED")
        f.find(720, 50, d["name"])
        a = QtGui.QGraphicsTextItem(d["name"])
        a.setDefaultTextColor(QtGui.QColor("black"))
        a.setFont(f)
        a.setPos(x + 202, y + 17)
        self.addItem(a)
        self.__items.append(a)
        a = QtGui.QGraphicsTextItem(d["name"])
        a.setDefaultTextColor(QtGui.QColor("white"))
        a.setFont(f)
        a.setPos(x + 200, y + 15)
        self.addItem(a)
        self.__items.append(a)
        for i in range(1, d["levels"] + 1):
            a = self.addPixmap(QtGui.QPixmap("resource/buttons/l{}-down.png".format(i)))
            a.setPos(x + 180 + 85 * (i - 1), y + 80)
            self.__items.append(a)
        a = self.addPixmap(QtGui.QPixmap("resource/levelblock.png"))
        a.setPos(x + 100, y + 10)
        self.__items.append(a)
        a = self.addPixmap(QtGui.QPixmap("resource/lock.png"))
        a.setPos(x + 120, y + 60)
        self.__items.append(a)
        return (x, y + 160)


    def getLevelSceneParent(self):
        return self.__parent