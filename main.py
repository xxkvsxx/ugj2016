#!/usr/bin/python3

from PyQt4 import QtGui
from gui import MainWindow
from gui import Achievement
from levels import lvl1b1n
from levels import lvl1b2n
from levels import lvl1b3n
from levels import lvl1b4n
from levels import lvl1b5n
from levels import lvl2b1n
from levels import lvl2b2n
from levels import lvl2b3n
from levels import lvl2b4n
from levels import lvl3b1n
from levels import lvl3b2n
from levels import lvl3b3n
from levels import lvl3b4n
from levels import lvl4b1n
from levels import lvl4b2n
from levels import lvl5b1n
from levels import lvl5b2n
from levels import lvl5b3n
import sys

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon("resource/icon.png"))
    QtGui.QFontDatabase.addApplicationFont("resource/hacked.ttf")
    QtGui.QFontDatabase.addApplicationFont("resource/candara.ttf")
    main = MainWindow.MainWindow()
    main.show()
    sys.exit(app.exec_())
